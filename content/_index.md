### Main Form

<div id="pipeline-form">
  <form action="#" method="post">
    <p>Choose your favorite dessert:</p>
    <select id="dessert">
      <option>Apple Pie</option>
      <option>Key Lime Pie</option>
      <option>Éclair</option>
      <option>Cheesecake</option>
      <option>Banana Pudding</option>
    </select>
    <button type="button" id="api-example">Run the Pipeline</button>
  </form>
</div>
<div id="pipeline-after" style="display:none;">
  <p id="api-output">Please stand by...</p>
</div>
<!-- <p><button id="api-example">Get Name and Bio from GitLab</button></p>
<div style="width:100%;background-color:#e0e0e0;padding:10px 10px 10px 10px;margin-bottom:40px;" id="api-output">Data will appear here</div> -->

